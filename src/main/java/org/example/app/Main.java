package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.handler.ApartmentHandler;
import org.example.app.handler.UserHandler;
import org.example.app.manager.ApartmentManager;
import org.example.app.manager.UserManager;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.middleware.AnonAuthMiddleware;
import org.example.framework.middleware.jsonbody.JsonBodyAuthMiddleware;
import org.example.framework.util.Maps;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;

@Slf4j
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        String password = new String(Files.readAllBytes(Paths.get("password.txt")));
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", password);

        final Gson gson = new Gson();

        final PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        final UserManager userManager = new UserManager(passwordEncoder);
        final UserHandler userHandler = new UserHandler(gson, userManager);

        final ApartmentManager manager = new ApartmentManager();
        final ApartmentHandler apartmentHandler = new ApartmentHandler(gson, manager);

        final String users = "^/users$";
        final String apartments = "^/apartments$";
        final String getById = "^/apartments/(?<apartmentId>[0-9]{1,9})$";
        final String update = "^/apartments/(?<apartmentId>[0-9]{1,9})/update$";
        final String delete = "^/apartments/(?<apartmentId>[0-9]{1,9})/delete$";

        final Server server = Server.builder()
                .middleware(new JsonBodyAuthMiddleware(userManager, gson))
                .middleware(new AnonAuthMiddleware())
                .routes(
                        Maps.of(
                                Pattern.compile(users), Maps.of(
                                        HttpMethods.POST, userHandler::register
                                ),
                                Pattern.compile(apartments), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getAll,
                                        HttpMethods.POST, apartmentHandler::create
                                ),
                                Pattern.compile(getById), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getById
                                ),
                                Pattern.compile(update), Maps.of(
                                        HttpMethods.POST, apartmentHandler::update
                                ),
                                Pattern.compile(delete), Maps.of(
                                        HttpMethods.POST, apartmentHandler::delete
                                )
                        )
                )
                .build();

        final int port = 8443;

        server.serve(port);

        Thread.sleep(100_000);

        server.stop();
    }
}
