package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApartmentRQ {
    private String numberOfRooms;
    private int price;
    private int area;
    private boolean balcony;
    private boolean loggia;
    private int floor;
    private int floorsInHouse;
}
