package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.app.domain.Apartment;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApartmentRS {
  private long id;
  private long created;
  private String numberOfRooms;
  private int price;
  private int area;
  private boolean balcony;
  private boolean loggia;
  private int floor;
  private int floorsInHouse;

  public ApartmentRS(Apartment apartment) {
    this.id = apartment.getId();
    this.created = apartment.getCreated();
    this.numberOfRooms = apartment.getNumberOfRooms();
    this.price = apartment.getPrice();
    this.area = apartment.getArea();
    this.balcony = apartment.isBalcony();
    this.loggia = apartment.isLoggia();
    this.floor = apartment.getFloor();
    this.floorsInHouse = apartment.getFloorsInHouse();
  }
}
