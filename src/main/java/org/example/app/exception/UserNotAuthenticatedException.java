package org.example.app.exception;

public class UserNotAuthenticatedException extends Exception {
    public UserNotAuthenticatedException() {
        super("user not authenticated");
    }
}
