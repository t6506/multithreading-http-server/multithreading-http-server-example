package org.example.app.exception;

public class UserNotAuthorizedException extends Exception {
    public UserNotAuthorizedException(String userName) {
        super("user " + userName + " not authorized for operation");
    }
}
