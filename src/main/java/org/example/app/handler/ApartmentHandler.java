package org.example.app.handler;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.*;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.exception.UserNotAuthenticatedException;
import org.example.app.exception.UserNotAuthorizedException;
import org.example.app.manager.ApartmentManager;
import org.example.framework.http.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class ApartmentHandler {
  private static final String OK = "200 Ok";
  private static final String BAD_REQUEST = "400 Bad Request";
  private static final String USER_NOT_AUTHORIZED_BODY = "{message: user not authorized}";
  private static final String USER_NOT_AUTHENTICATED_BODY = "{message: user not authenticated}";
  private static final String ITEM_NOT_FOUND_BODY = "{message: not found}";
  private static final String APARTMENT_ID_PATH = "apartmentId";
  private final Gson gson;
  private final ApartmentManager manager;

  public void getAll(final Request request, final OutputStream responseStream) throws IOException {
    final List<ApartmentRS> responseDTO = manager.getAll();
    final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
    writeResponse(responseStream, OK, responseBody);
  }

  public void create(final Request request, final OutputStream responseStream) throws IOException {
    try {
      final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
      final ApartmentRQ createRQ = gson.fromJson(requestBody, ApartmentRQ.class);
      final ApartmentRS createRS = manager.create(createRQ);
      final byte[] responseBody = gson.toJson(createRS).getBytes(StandardCharsets.UTF_8);
      writeResponse(responseStream, OK, responseBody);
    }
    catch (UserNotAuthorizedException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHORIZED_BODY.getBytes(StandardCharsets.UTF_8));
    }
    catch (UserNotAuthenticatedException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHENTICATED_BODY.getBytes(StandardCharsets.UTF_8));
    }
  }

  public void getById(final Request request, final OutputStream responseStream) throws IOException {
    try {
      final int id = Integer.parseInt(request.getPathGroup(APARTMENT_ID_PATH));
      final byte[] responseBody = gson.toJson(manager.getById(id)).getBytes(StandardCharsets.UTF_8);
      writeResponse(responseStream, OK, responseBody); }
    catch (ItemNotFoundException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, ITEM_NOT_FOUND_BODY.getBytes(StandardCharsets.UTF_8));
    }
  }

  public void update(final Request request, final OutputStream responseStream) throws IOException {
    try {
      final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
      final int id = Integer.parseInt(request.getPathGroup(APARTMENT_ID_PATH));
      final ApartmentRQ updateRQ = gson.fromJson(requestBody, ApartmentRQ.class);
      final ApartmentRS updateRS = manager.update(updateRQ, id);
      final byte[] responseBody = gson.toJson(updateRS).getBytes(StandardCharsets.UTF_8);
      writeResponse(responseStream, OK, responseBody); }
    catch (ItemNotFoundException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, ITEM_NOT_FOUND_BODY.getBytes(StandardCharsets.UTF_8));
    }
    catch (UserNotAuthenticatedException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHENTICATED_BODY.getBytes(StandardCharsets.UTF_8));
    }
    catch (UserNotAuthorizedException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHORIZED_BODY.getBytes(StandardCharsets.UTF_8));
    }
  }

  public void delete(final Request request, final OutputStream responseStream) throws IOException {
    try {
      final int id = Integer.parseInt(request.getPathGroup(APARTMENT_ID_PATH));
      manager.removeById(id);
      writeResponse(responseStream, OK, ("{ message: \" remove item by id " + id + " success \"}").getBytes(StandardCharsets.UTF_8)); }
    catch (ItemNotFoundException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, ITEM_NOT_FOUND_BODY.getBytes(StandardCharsets.UTF_8));
    }
    catch (UserNotAuthenticatedException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHENTICATED_BODY.getBytes(StandardCharsets.UTF_8));
    }
    catch (UserNotAuthorizedException e) {
      log.debug(e.getMessage());
      writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHORIZED_BODY.getBytes(StandardCharsets.UTF_8));
    }
  }

  private void writeResponse(OutputStream responseStream, String status, byte[] body) throws IOException {
    responseStream.write((
        "HTTP/1.1 " + status + "\r\n" +
        "Content-Length: " + body.length + "\r\n" +
        "Connection: close\r\n" +
        "Content-Type: application/json\r\n" +
        "\r\n"
    ).getBytes(StandardCharsets.UTF_8));
    responseStream.write(body);
  }
}
