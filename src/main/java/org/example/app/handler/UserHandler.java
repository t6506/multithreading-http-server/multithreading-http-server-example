package org.example.app.handler;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.exception.LoginAlreadyRegisteredException;
import org.example.app.manager.UserManager;
import org.example.framework.http.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
public class UserHandler {
    private static final String OK = "200 Ok";
    private static final String BAD_REQUEST = "400 Bad Request";
    private static final String LOGIN_ALREADY_REGISTERED_BODY = "{message: login already registered}";
    private final Gson gson;
    private final UserManager manager;

    public void register(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final UserRegisterRQ registerRQ = gson.fromJson(requestBody, UserRegisterRQ.class);

        final UserRegisterRS registerRS;
        try {
            registerRS = manager.create(registerRQ);
        final byte[] responseBody = gson.toJson(registerRS).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, OK, responseBody);
        } catch (LoginAlreadyRegisteredException e) {
            log.debug(e.getMessage());
            writeResponse(responseStream, BAD_REQUEST, LOGIN_ALREADY_REGISTERED_BODY.getBytes(StandardCharsets.UTF_8));
        }
    }

    private void writeResponse(OutputStream responseStream, String status, byte[] body) throws IOException {
        responseStream.write((
                "HTTP/1.1 " + status + "\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: application/json\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }
}
