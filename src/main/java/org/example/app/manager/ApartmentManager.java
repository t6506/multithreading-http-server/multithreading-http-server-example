package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.Apartment;
import org.example.app.dto.ApartmentRQ;
import org.example.app.dto.ApartmentRS;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.exception.UserNotAuthenticatedException;
import org.example.app.exception.UserNotAuthorizedException;
import org.example.framework.auth.SecurityContext;
import org.example.framework.auth.principal.AnonymousPrincipal;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class ApartmentManager {
    private long nextId = 1;
    private final List<Apartment> items = new ArrayList<>();

    public ApartmentRS create(final ApartmentRQ createRQ) throws UserNotAuthorizedException, UserNotAuthenticatedException {
        if (SecurityContext.getPrincipal().getName().equals(AnonymousPrincipal.ANONYMOUS)) {
            throw new UserNotAuthenticatedException();
        }
        synchronized (this) {
            final Apartment item = new Apartment(
                    nextId,
                    SecurityContext.getPrincipal().getName(),
                    Instant.now().getEpochSecond(),
                    createRQ.getNumberOfRooms(),
                    createRQ.getPrice(),
                    createRQ.getArea(),
                    createRQ.isBalcony(),
                    createRQ.isLoggia(),
                    createRQ.getFloor(),
                    createRQ.getFloorsInHouse()
            );
            log.debug("create item: {}", item);
            items.add(item);
            nextId += 1;
            return new ApartmentRS(item);
        }
    }

    public ApartmentRS getById(final long id) throws ItemNotFoundException{
        synchronized (this) {
            for (final Apartment item : items) {
                if (item.getId() == id) {
                    log.debug("return item: {}", item);
                    return new ApartmentRS(item);
                }
            }
        }
        throw new ItemNotFoundException(id);
    }

    public synchronized List<ApartmentRS> getAll() {
        return items.stream()
                .map(ApartmentRS::new)
                .collect(Collectors.toList());
    }

    public void removeById(final long id) throws
            ItemNotFoundException, UserNotAuthenticatedException, UserNotAuthorizedException {
        if (SecurityContext.getPrincipal().getName().equals(AnonymousPrincipal.ANONYMOUS)) {
            throw new UserNotAuthenticatedException();
        }
        final boolean removed;
        synchronized (this) {
            if (!getOwner(id).equals(SecurityContext.getPrincipal().getName())) {
                throw new UserNotAuthorizedException(SecurityContext.getPrincipal().getName());
            }
            removed = items.removeIf(o -> o.getId() == id);
        }
        if (!removed) {
            throw new ItemNotFoundException(id);
        }
        log.debug("remove item by id {} success", id);
    }

    public ApartmentRS update(final ApartmentRQ updateRQ, final long id)
            throws ItemNotFoundException, UserNotAuthenticatedException, UserNotAuthorizedException {
        if (SecurityContext.getPrincipal().getName().equals(AnonymousPrincipal.ANONYMOUS)) {
            throw new UserNotAuthenticatedException();
        }
        synchronized (this) {
            if (!getOwner(id).equals(SecurityContext.getPrincipal().getName())) {
                throw new UserNotAuthorizedException(SecurityContext.getPrincipal().getName());
            }
            final ApartmentRS item = getById(id);
            final Apartment updatedItem = new Apartment(
                    id,
                    getOwner(id),
                    item.getCreated(),
                    updateRQ.getNumberOfRooms(),
                    updateRQ.getPrice(),
                    updateRQ.getArea(),
                    updateRQ.isBalcony(),
                    updateRQ.isLoggia(),
                    updateRQ.getFloor(),
                    updateRQ.getFloorsInHouse()
            );
            items.set(getIndexById(id), updatedItem);
            log.debug("update item id {}", id);
            return new ApartmentRS(updatedItem);
        }
    }

    private int getIndexById(final long id) {
        for (int i = 0; i < items.size(); i++) {
            final Apartment apartment = items.get(i);
            if (apartment.getId() == id) {
                log.debug("return index: {} by id: {}", i, id);
                return i;
            }
        }
        log.debug("index by id: {} not found", id);
        return -1;
    }

    private String getOwner(final long id) throws ItemNotFoundException{
        for (final Apartment item : items) {
            if (item.getId() == id) {
                log.debug("return owner: {}", item.getOwner());
                return item.getOwner();
            }
        }
        throw new ItemNotFoundException(id);
    }
}
