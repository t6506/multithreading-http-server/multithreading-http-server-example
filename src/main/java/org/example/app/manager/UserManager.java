package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.exception.LoginAlreadyRegisteredException;
import org.example.framework.auth.AuthenticationToken;
import org.example.framework.auth.Authenticator;
import org.example.framework.auth.LoginPasswordAuthenticationToken;
import org.example.framework.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashMap;
import java.util.Map;
@Slf4j
@RequiredArgsConstructor
public class UserManager implements Authenticator {
    private final Map<String, String> users = new HashMap<>();
    private final PasswordEncoder encoder;

    public UserRegisterRS create(final UserRegisterRQ createRQ) throws LoginAlreadyRegisteredException{
        final String login = createRQ.getLogin().trim().toLowerCase();
        final String encodePassword = encoder.encode(createRQ.getPassword());

        synchronized (this) {
            if (users.containsKey(login)) {
                throw new LoginAlreadyRegisteredException(login);
            }
            users.put(login, encodePassword);
            log.debug("create user: {}", login);
            return new UserRegisterRS(login);
        }
    }

    @Override
    public boolean authenticate(final AuthenticationToken request) throws UnsupportAuthenticationToken{
        if (!(request instanceof LoginPasswordAuthenticationToken)) {
            throw new UnsupportAuthenticationToken();
        }

        final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();
        final String encodedPassword;

        synchronized (this) {
            if (!users.containsKey(request.getLogin())) {
                return false;
            }
            encodedPassword = users.get(login);
        }
        return encoder.matches(password, encodedPassword);
    }
}
